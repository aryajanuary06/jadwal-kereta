<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Tambah</title>
    <!-- Link to Bootstrap CSS -->
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css">
</head>
<body class="container" style="max-width: 900px; border: 2px solid #ddd; padding: 20px;">

    <div class="mt-3">
        <h2>Tambah Jadwal Kereta</h2>
    </div>

    <?php echo form_open('kereta/store', 'class="mt-3"'); ?>

    <div class="form-group">
        <label for="nomer">Nomer Perjalanan:</label>
        <input type="text" name="nomer" class="form-control"  placeholder="Contoh: 123">
    </div>
    <div class="form-group">
        <label for="nama">Nama Kereta:</label>
        <input type="text" name="nama" class="form-control" required placeholder="Contoh: Progo">
    </div>
    <div class="form-group">
        <label for="kelas">Kelas Kereta:</label>
        <input type="text" name="kelas" class="form-control" required placeholder="Contoh: Ekonomi">
    </div>
    <div class="form-group">
        <label for="asal">Stasiun Asal:</label>
        <input type="text" name="asal" class="form-control" required placeholder="Contoh: Gambir">
    </div>
    <div class="form-group">
        <label for="jurusan">Stasiun Tujuan:</label>
        <input type="text" name="jurusan" class="form-control" required placeholder="Contoh: Kroya">
    </div>
    <div class="form-group">
        <label for="keberangkatan">Keberangkatan:</label>
        <input type="time" name="keberangkatan" class="form-control" placeholder="Contoh: 14:30 WIB" required>
    </div>
    <div class="form-group">
        <label for="waktu_tiba">Waktu Tiba:</label>
        <input type="time" name="waktu_tiba" class="form-control" placeholder="Contoh: 14:30 WIB" required>
    </div>

    <div class="mt-3">
        <button type="button" onclick="window.history.back();" class="btn btn-secondary">Kembali</button>
        <button type="submit" class="btn btn-primary">Simpan</button>
    </div>

    <?php echo form_close(); ?>

    <!-- Include Bootstrap JS and jQuery (required by Bootstrap) if used -->
    <script src="https://code.jquery.com/jquery-3.3.1.slim.min.js"></script>
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js"></script>

</body>
</html>
