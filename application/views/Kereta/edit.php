<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Edit Kereta</title>
    <!-- Add Bootstrap CSS link -->
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.5.2/css/bootstrap.min.css">
</head>
<body class="container" style="max-width: 900px; border: 2px solid #ddd; padding: 20px;">

<div class="container mt-5">
    <h2>Edit Informasi Kereta</h2>

    <form action="<?php echo base_url('kereta/update/' . $kereta->id); ?>" method="post">
        <div class="form-group">
            <label for="nomer">Nomer Perjalanan Kereta :</label>
            <input type="text" class="form-control" id="nomer" name="nomer" value="<?php echo $kereta->nomer; ?>" required>
        </div>
        <div class="form-group">
            <label for="nama">Nama Kereta :</label>
            <input type="text" class="form-control" id="nama" name="nama" value="<?php echo $kereta->nama; ?>" required>
        </div>
        <div class="form-group">
            <label for="kelas">Kelas Kereta :</label>
            <input type="text" class="form-control" id="kelas" name="kelas" value="<?php echo $kereta->kelas; ?>" required>
        </div>
        <div class="form-group">
            <label for="asal"> Stasiun Asal :</label>
            <input type="text" class="form-control" id="asal" name="asal" value="<?php echo $kereta->asal; ?>" required>
        </div>
        <div class="form-group">
            <label for="jurusan">Stasiun Tujuan :</label>
            <input type="text" class="form-control" id="jurusan" name="jurusan" value="<?php echo $kereta->jurusan; ?>" required>
        </div>
        <div class="form-group">
            <label for="keberangkatan">Keberangkatan</label>
            <input type="time" class="form-control" id="keberangkatan" name="keberangkatan" value="<?php echo $kereta->keberangkatan; ?>" required>
        </div>
        <div class="form-group">
            <label for="waktu_tiba">Waktu Tiba:</label>
            <input type="time" class="form-control" id="waktu_tiba" name="waktu_tiba" value="<?php echo $kereta->waktu_tiba; ?>" required>
        </div>
        
        <div class="mb-3">
            <button type="button" onclick="window.history.back();" class="btn btn-secondary">Kembali</button>
            <button type="submit" class="btn btn-primary">Simpan</button>
        </div>
    </form>
</div>

<!-- Add Bootstrap JS and Popper.js scripts -->
<script src="https://code.jquery.com/jquery-3.5.1.slim.min.js"></script>
<script src="https://cdn.jsdelivr.net/npm/@popperjs/core@2.5.2/dist/umd/popper.min.js"></script>
<script src="https://stackpath.bootstrapcdn.com/bootstrap/4.5.2/js/bootstrap.min.js"></script>

</body>
</html>
