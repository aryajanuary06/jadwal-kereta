<!-- application/views/keretaind/index.php -->

<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>KAI</title>
    <!-- Link to Bootstrap CSS -->
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css">
    <!-- Link to DataTables CSS -->
    <link rel="stylesheet" href="https://cdn.datatables.net/1.10.21/css/dataTables.bootstrap4.min.css">
    <!-- Link to SweetAlert CSS -->
    <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/sweetalert2@10">
    <style>
    /* Style untuk menambahkan border pada tabel */
    #myTable {
        border: 2px solid #ddd;
        border-collapse: collapse;
        margin-top: 15px;
    }

    #myTable th,
    #myTable td {
        border: 1px solid #ddd;
        padding: 8px;
        text-align: left;
    }

    /* Add animation for blinking text */
    @keyframes blink {
        0%, 50%, 100% {
            opacity: 1;
        }
        25%, 75% {
            opacity: 0;
        }
    }

    #advertisement {
        animation: blink 1s infinite; /* Apply the blink animation to the advertisement text */
    }
</style>


    <!-- Script untuk menampilkan pesan sukses selama 5 detik -->
    <script src="https://code.jquery.com/jquery-3.3.1.slim.min.js"></script>
    <script src="https://cdn.jsdelivr.net/npm/sweetalert2@10"></script>
    <script>
        $(document).ready(function() {
            // Tampilkan pesan jika ada
            <?php if ($this->session->flashdata('success')) : ?>
                showAlert("<?php echo $this->session->flashdata('success'); ?>");
                <?php $this->session->set_flashdata('success', ''); ?> // Mengosongkan flashdata setelah ditampilkan
            <?php endif; ?>

            // Fungsi untuk menampilkan pesan dengan SweetAlert
            function showAlert(message) {
                Swal.fire({
                    title: message,
                    icon: 'success',
                    timer: 5000, // Menampilkan pesan selama 5 detik
                    showConfirmButton: false
                }).then(() => {
                    // Hapus SweetAlert setelah ditutup
                    $('#swal2-container').remove();
                });
            }

            // Update the advertisement every 10 seconds (adjust the interval as needed)
            // Update the advertisement every 10 seconds (adjust the interval as needed)
            function updateAdvertisement() {
                var currentTime = new Date();
                var oneMinuteBefore = new Date(currentTime.getTime() + 1 * 60000); // 1 minute in milliseconds
                

                var upcomingTrains = <?php echo json_encode($keretaind); ?>.filter(function(kereta) {
                    var timeKereta = new Date(currentTime.toDateString() + ' ' + kereta.keberangkatan);
                    return timeKereta > currentTime && timeKereta <= oneMinuteBefore;
                }).sort(function(a, b) {
                    var timeA = new Date(currentTime.toDateString() + ' ' + a.keberangkatan);
                    var timeB = new Date(currentTime.toDateString() + ' ' + b.keberangkatan);
                    return timeA - timeB;
                });

                var advertisementContent = '';

                upcomingTrains.forEach(function(kereta) {
                    advertisementContent += 'Nomor KA : ' + kereta.nomer + ' - ';
                    advertisementContent += 'Nama KA : ' + kereta.nama + ' - ';
                    advertisementContent += 'Keberangkatan: ' + kereta.keberangkatan + ' WIB - ';
                    advertisementContent += 'Jurusan: ' + kereta.jurusan + '  ';

                    // Play notification sound only 1 minute before departure
                    if (isWithinOneMinute(kereta.keberangkatan)) {
                        playNotificationSound();
                    }
                });

                document.getElementById('advertisement').innerHTML = advertisementContent;
            }

            function isWithinOneMinute(keberangkatan) {
                var currentTime = new Date();
                var departureTime = new Date(currentTime.toDateString() + ' ' + keberangkatan);
                var oneMinuteBefore = new Date(departureTime.getTime() - 1 * 60000); // 1 minute before departure

                return currentTime >= oneMinuteBefore && currentTime < departureTime;
            }

            function playNotificationSound() {
                var audio = new Audio('public/sounds/bel_stasiun_kereta.mp3');
                audio.play();
            }




            // Update the advertisement initially
            updateAdvertisement();

            // Update the advertisement every 10 seconds
            setInterval(updateAdvertisement, 10000);

        });
    </script>
</head>

<body class="container">

    <!-- Header Section -->
    <div class="jumbotron mt-3">
        <h1 class="display-4">INFORMASI PERJALANAN KERETA</h1>
        <img src="<?php echo base_url('public/image/logo kai.png'); ?>" alt="Train Image" class="img-fluid mt-3" style="max-height: 200px;">
        <p class="lead">Selamat datang di halaman jadwal kereta KAI.</p>
        <hr class="my-4">
        <a href="<?php echo base_url('Kereta/create'); ?>" class="btn btn-primary btn-lg">Tambah Kereta</a>




        <!-- Display Current Time -->
        <!-- <p id="currentTime" class="ml-3" style="font-size: 20px;"></p> -->
    </div>

    <!-- ... (rest of your code) -->


    <div class="mt-3">
        <marquee behavior="scroll" direction="left" scrollamount="3">
            <span id="advertisement"></span>
        </marquee>
    </div>



    <!-- Table Section -->
    <table id="myTable" class="table table-bordered mt-3">
        <thead class="thead-dark">
            <tr>
                <th>ID</th>
                <th>Nomer KA</th>
                <th>Nama Kereta</th>
                <th>Kelas Kereta</th>
                <th>Asal</th>
                <th>Jurusan</th>
                <th>Keberangkatan</th>
                <th>Waktu Tiba</th>
                <th>Aksi</th>
            </tr>
        </thead>
        <tbody>
            <?php foreach ($keretaind as $index => $kereta) { ?>
                <tr>
                    <td><?php echo $index + 1; ?></td>
                    <td><?php echo $kereta->nomer; ?></td>
                    <td><?php echo $kereta->nama; ?></td>
                    <td><?php echo $kereta->kelas; ?></td>
                    <td><?php echo $kereta->asal; ?></td>
                    <td><?php echo $kereta->jurusan; ?></td>
                    <td><?php echo $kereta->keberangkatan . ' WIB'; ?></td>
                    <td><?php echo $kereta->waktu_tiba . ' WIB'; ?></td>
                    <td>
                        <a href="<?php echo base_url('kereta/edit/' . $kereta->id); ?>" class="btn btn-warning btn-sm">Edit</a>
                        <a href="<?php echo base_url('kereta/delete/' . $kereta->id); ?>" class="btn btn-danger btn-sm" onclick="return confirm('Apakah Anda yakin ingin menghapus?')">Hapus</a>
                    </td>
                </tr>
            <?php } ?>
        </tbody>
    </table>

    <!-- Include Bootstrap JS and DataTables JS (required by Bootstrap and DataTables) -->
    <script src="https://code.jquery.com/jquery-3.3.1.slim.min.js"></script>
    <script src="https://cdn.datatables.net/1.10.21/js/jquery.dataTables.min.js"></script>
    <script src="https://cdn.datatables.net/1.10.21/js/dataTables.bootstrap4.min.js"></script>

    <!-- DataTables Initialization -->
    <script>
        $(document).ready(function() {
            $('#myTable').DataTable({
                "ordering": false // Menonaktifkan ordering (panah pada header kolom)
            });
        });
    </script>

</body>

</html>