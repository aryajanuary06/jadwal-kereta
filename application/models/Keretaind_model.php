<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Keretaind_model extends CI_Model {

    public function get_all_data() {
        return $this->db->get('keretaind')->result();
    }

    public function insert_data() {
        $data = array(
            'nomer' => $this->input->post('nomer'),
            'nama' => $this->input->post('nama'),
            'kelas' => $this->input->post('kelas'),
            'asal' => $this->input->post('asal'),
            'jurusan' => $this->input->post('jurusan'),
            'keberangkatan' => $this->input->post('keberangkatan'),
            'waktu_tiba' => $this->input->post('waktu_tiba')
        );
        $this->db->insert('keretaind', $data);
    }

    public function get_data_by_id($id) {
        return $this->db->get_where('keretaind', array('id' => $id))->row();
    }

    public function update_data($id) {
        $data = array(
            'nomer' => $this->input->post('nomer'),
            'nama' => $this->input->post('nama'),
            'kelas' => $this->input->post('kelas'),
            'asal' => $this->input->post('asal'),
            'jurusan' => $this->input->post('jurusan'),
            'keberangkatan' => $this->input->post('keberangkatan'),
            'waktu_tiba' => $this->input->post('waktu_tiba')
        );
        $this->db->where('id', $id);
        $this->db->update('keretaind', $data);
    }

    public function delete_data($id) {
        $this->db->where('id', $id);
        $this->db->delete('keretaind');
    }

    
}
