<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Kereta extends CI_Controller {

    public function __construct() {
        parent::__construct();
        $this->load->model('keretaind_model');
    }

    public function index() {
        $data['keretaind'] = $this->keretaind_model->get_all_data();
        $this->load->view('Kereta/List', $data);
    }

    public function create() {
        // Tampilkan form untuk menambah data
        $this->load->view('Kereta/create');
    }

    public function store() {
        // Simpan data ke database
        $this->keretaind_model->insert_data();
        $this->session->set_flashdata('success', 'Data kereta berhasil ditambahkan.');

        redirect('Kereta');
    }

    public function edit($id) {
        // Tampilkan form untuk mengedit data
        $data['kereta'] = $this->keretaind_model->get_data_by_id($id);
        $this->load->view('Kereta/edit', $data);
    }

    public function update($id) {
        // Update data ke database
        $this->keretaind_model->update_data($id);
        $this->session->set_flashdata('success', 'Data kereta berhasil di ubah.');
        redirect('Kereta');
    }

    public function delete($id) {
        // Hapus data dari database
        $this->keretaind_model->delete_data($id);
        $this->session->set_flashdata('success', 'Data kereta berhasil di hapus.');
        redirect('Kereta');
    }
}
